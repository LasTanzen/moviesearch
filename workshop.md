# Angular Workshop

## create and run the project
- get [nodejs](https://nodejs.org/en/)
- get the [cli](https://github.com/angular/angular-cli): `npm install -g angular-cli`
- create the project `ng new moviesearch`
- open the project `cd moviesearch`
- run the dev server `ng serve`
- open the app in your browser at `http://localhost:4200/`
- check out the current code

## change the app component
- alter title to **Moviesearch** 
- adjust the created tests in *app.component.spec.ts*
- (close your browser and run test with `ng test`)

## create a header component
- create component with `ng g component header`
- check out *app.module.ts*, `HeaderComponent` was added to `declarations`
- open *app.component.html* and add `app-header` tag to the top
- move title from *app.component* to *header.component*

## add some styling
- for the sake of ease in this example we get our bootstrap from CDN rather then a custom build with the *boostrap-sass* package from npm:
  - add the style link to *index.html* (inside `head`) 

```html
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
```

- add markup for bootstraps *fixed-top* navbar to the *header.component*:

```html
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <span class="navbar-brand">
          Moviesearch
        </span>
      </div>
    </div>
  </nav>
``` 

- add some dummy content to the app component

```html
<app-header></app-header>
<p>content</p>
```

- add height to the component in *header.component.css* to prevent overlapping

```css
:host {
  display: block;
  height: 70px;
}
```

## Create a search module
- create the module with `ng g module search`
- add the search component to the search modules exports in ordert to use it in other modules

```typescript
@NgModule({
  ...
  exports: [SearchComponent]
})
``` 

- import the search module in *app.module.ts*

```typescript
import { SearchModule } from './search/search.module';
...
@NgModule({
  ...
  imports: [
    ...
    SearchModule
  ],
  ...
}) 
``` 

- add the search component to *app.component.html*

## Create the search input
- generate a the component inside the search module with `ng g component search/search-input`
- add the search input component to the search component
- add a text input to the search input component

```html
<div class="form-group">
  <input type="text" class="form-control" placeholder="search">
</div>
```

- add reactive form module to the search module

```typescript
import { ReactiveFormsModule } from '@angular/forms';
...
@NgModule({
  imports: [
    ReactiveFormsModule
    ...
  ]
  ...
})
```

- create a form control for the search input

```typescript
searchControl = new FormControl();
```

- link it to the input

```html
<input type="text" class="form-control" placeholder="search" [formControl]="searchControl">
```

- and listen to its changes

```typescript
ngOnInit() {
  this.searchControl.valueChanges.subscribe(value => console.log(value));
}
```

- add an event emitter

```typescript
@Output('update') update = new EventEmitter<string>();
```

- now we can emit our inputs changes 

```typescript
this.searchControl.valueChanges.subscribe(value => this.update.emit(value));
```

- we may want to optimize this with the power of rxjs (add a debounce of 200ms, to be nice to the api)

```typescript
this.searchControl.valueChanges
  .debounceTime(200)
  .subscribe(value => this.update.emit(value));
```

- now listen for that at the search component

```html
<app-search-input (update)="onSearchInput($event)"></app-search-input>
```
```typescript
onSearchInput(value: string) {
  // do something with the value
}
```

## Create a service to call the api
- create the service with `ng g service search/movies`
- the cli tells us what to do next: `WARNING Service is generated but not provided, it must be provided to be used`

```typescript
@NgModule({
  ...
  providers: [MoviesService]
})
```

- we also need a value provider for the movie database api we use, create one like explained here: https://developers.themoviedb.org/3/getting-started
- then create a file `key.ts` in your `search` folder in which you export this key like:

```typescript
export const key: string = '-- your key here --';
```

- to use this as a value provider we need to create an injection token, therefor we create a file `search-settings` in the same folder and export an injection token from there:

```typescript
import { InjectionToken } from '@angular/core';

export const apiKey = new InjectionToken('MovieDbApiKey');
```

- now we can provide this value in our module:

```typescript
import { apiKey } from './search-settings';
import { key } from './key';
...
@NgModule({
  ...
  providers: [
    MoviesService,
    {
      provide: apiKey,
      useValue: key
    }
  ]
})
```

- with this technique we can prevent globals values, maybe you want to put the value file somewhere else or put the value in your environment files, but make sure to remove this from source control (e.g. `.gitignore`)

- angulars http module (and its providers) is already imported inside the app module while scaffolding
- inject angulars http service and the api key into the movies service

```typescript
import { Injectable, Inject } from '@angular/core';
...
import { apiKey } from './search-settings';
...
constructor(private http: Http, @Inject(apiKey) private key: string) { }
```

- create movie model with `ng g interface movie`

```typescript
export interface Movie {
  title: string,
  type: string,
  releaseDate: string,
  poster: string
}
```

- create search function

```typescript
private apiUrl = 'https://api.themoviedb.org/3';
private imageUrl = 'http://image.tmdb.org/t/p/w92';
...
search(term: string): Observable<Movie[]> {
    // return empty result when no term given
    if (!term) {
      return Observable.of([]);
    }
    
    const query = encodeURIComponent(term);
    return this.http.get(`${this.apiUrl}/search/movie?api_key=${this.key}&query=${query}&include_adult=false`)
    .map(res => {
      const result = res.json();
      // map to movie model
      return result.results.map(resMovie => ({
        title: resMovie.title,
        releaseDate: resMovie.release_date,
        poster: resMovie.poster_path ? `${this.imageUrl}/${resMovie.poster_path}` : null
      }));
    });
  }
```

- inject movies service in search component

```typescript
constructor(private moviesService: MoviesService) { }
```

- subscribe to movie service search function on input updates

```typescript

onSearchInput(value: string) {
  this.moviesService.search(value).first()
    .subscribe(res => console.log(res));
}
```

## Show the results
- create a movie list component with `ng g component search/movie-list`
- add a movies input to the component

```typescript
@Input('movies') movies: Movie[] = [];
```

- add it to the search component

```typescript
movies: Movie[] = [];
...
onSearchInput(value: string) {
  this.moviesService.search(value).first()
    .subscribe(res => this.movies = res);
}
```
```html
<app-search-input (update)="onSearchInput($event)"></app-search-input>
<app-movie-list [movies]="movies"></app-movie-list>
```

- add some markup to the movie list

```html
<div class="media" *ngFor="let movie of movies">
  <div class="media-left">
    <img class="media-object" [src]="movie.poster" *ngIf="movie.poster">
  </div>
  <div class="media-body">
    <h4 class="media-heading">{{ movie.title }}</h4>
    Release Date: {{ movie.releaseDate }}
  </div>
</div>
<div class="well" *ngIf="!movies.length">
  No movies availible!
</div>
``` 

- style the component

```css
.media {
  border-bottom: 1px solid #999;
  padding-bottom: 5px;
}
.media-object {
  max-height: 50px;
}
```

## Add 'My Movies' with store (optional - explain redux)
- create shared module `ng g module shared`
- move movies list to shared module folder
- delete shared component
- edit *search.module.ts*

```typescript
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [SearchComponent, SearchInputComponent],
  exports: [SearchComponent],
  providers: [MoviesService]
})
export class SearchModule { }
```

- *shared.module.ts*

```typescript
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MovieListComponent],
  exports: [MovieListComponent]
})
export class SharedModule { }
```

- create my movies module `ng g module my-movies`
- export my movies component

```typescript
exports: [MyMoviesComponent]
```

- import to app module and add component

```typescript
...
imports: [
  ...
  MyMoviesModule
],
...
```
```html
<app-header></app-header>
<app-search></app-search>
<app-my-movies></app-my-movies>
```

- get `@ngrx/store` with `npm install @ngrx/core @ngrx/store --save`
- create app state model `ng g interface app-state`

```typescript
import { Movie } from './movie';

export interface AppState {
  myMovies: Movie[]
}
```

- create file *my-movies/my-movies.reducer.ts*

```typescript
export const ADD = '[MyMovies] Add';
export const REMOVE = '[MyMovies] Remove';

export function myMoviesReducer(state = [], action: Action): Movie[] {
  switch (action.type) {
    case ADD:
      if (!state.find(movie => movie.title === action.payload.title)) {
        state.push(action.payload);
      }
      return state;
    case REMOVE:
      let i;
      state.forEach((movie, index) => {
        if(action.payload.title === movie.title) {
          i = index;
        }
      });
      state.splice(i, 1);
      return state;
    default:
      return state;
  }
}
```

- import store module and provide with reducer in *app.module.ts*

```typescript
...
imports: [
  ...
  StoreModule.provideStore({
    myMovies: myMoviesReducer
  })
],
...
```

- add interaction to movie list

```typescript
...
@Output('select') select = new EventEmitter<Movie>();
...
onSelect(movie: Movie) {
  this.select.emit(movie);
}
...
```
```html
<div class="media" *ngFor="let movie of movies" (click)="onSelect(movie)">
...
```
```css
.media:hover {
  background-color: #eee;
  cursor: pointer;
}
```

- update store on movie list clicks in search component

```html
<app-movie-list [movies]="movies" (select)="onMovieSelect($event)"></app-movie-list>
```
```typescript
...
constructor(private moviesService: MoviesService, private store: Store<AppState>) { }
...
onMovieSelect(movie: Movie) {
  this.store.dispatch({
    type: ADD,
    payload: movie
  });
}
...
```

- get app state Observable in my movies component

```typescript
...
myMovies$ = this.store.select(state => state.myMovies);

constructor(private store: Store<AppState>) { }
...
```

- import shared module to my movies module

```typescript
...
imports: [
  ...
  SharedModule
],
...
```

- add list with selected movies to my movies component using async pipe

```html
<app-movie-list [movies]="myMovies$ | async"></app-movie-list>
```

- use store to remove from my movies

```typescript
...
onMovieSelect(movie: Movie) {
  this.store.dispatch({
    type: REMOVE,
    payload: movie
  });
}
...
```
```html
<app-movie-list [movies]="myMovies$ | async" (select)="onMovieSelect($event)"></app-movie-list>
```
- NEXT: move search to store, make everything more typesafe with actions classes, do api calls with @ngrx/effects
