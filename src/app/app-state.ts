import { Movie } from './movie';

export interface AppState {
  myMovies: Movie[]
}
