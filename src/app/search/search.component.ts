import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { MoviesService } from './movies.service';
import { Movie } from '../movie';
import { AppState } from '../app-state';
import { ADD } from '../my-movies/my-movies.reducer';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  movies: Movie[] = [];

  constructor(private moviesService: MoviesService, private store: Store<AppState>) { }

  ngOnInit() {
  }

  onSearchInput(value: string) {
    this.moviesService.search(value).first()
      .subscribe(res => this.movies = res);
  }

  onMovieSelect(movie: Movie) {
    this.store.dispatch({
      type: ADD,
      payload: movie
    });
  }

}
