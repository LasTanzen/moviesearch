import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements OnInit {

  @Output('update') update = new EventEmitter<string>();

  searchControl = new FormControl();

  constructor() { }

  ngOnInit() {
    this.searchControl.valueChanges
      .debounceTime(200)
      .subscribe(value => this.update.emit(value));
  }

}
