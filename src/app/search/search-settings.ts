import { InjectionToken } from '@angular/core';

export const apiKey = new InjectionToken('MovieDbApiKey');
