import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Movie } from '../movie';
import { apiKey } from './search-settings';

@Injectable()
export class MoviesService {

  private apiUrl = 'https://api.themoviedb.org/3';
  private imageUrl = 'http://image.tmdb.org/t/p/w92';

  constructor(private http: Http, @Inject(apiKey) private key: string) { }

  search(term: string): Observable<Movie[]> {
    // return empty result when no term given
    if (!term) {
      return Observable.of([]);
    }
    
    const query = encodeURIComponent(term);
    return this.http.get(`${this.apiUrl}/search/movie?api_key=${this.key}&query=${query}&include_adult=false`)
    .map(res => {
      const result = res.json();
      // map to movie model
      return result.results.map(resMovie => ({
        title: resMovie.title,
        releaseDate: resMovie.release_date,
        poster: resMovie.poster_path ? `${this.imageUrl}/${resMovie.poster_path}` : null
      }));
    });
  }

}
