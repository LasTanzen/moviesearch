import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SearchComponent } from './search.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { MoviesService } from './movies.service';
import { SharedModule } from '../shared/shared.module';
import { apiKey } from './search-settings';
import { key } from './key';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [SearchComponent, SearchInputComponent],
  exports: [SearchComponent],
  providers: [
    MoviesService,
    {
      provide: apiKey,
      useValue: key
    }
  ]
})
export class SearchModule { }
