export interface Movie {
  title: string,
  releaseDate: string,
  poster: string
}
