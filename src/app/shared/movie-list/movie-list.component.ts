import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../../movie';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  @Input('movies') movies: Movie[] = [];

  @Output('select') select = new EventEmitter<Movie>();

  constructor() { }

  ngOnInit() {
  }

  onSelect(movie: Movie) {
    this.select.emit(movie);
  }

}
