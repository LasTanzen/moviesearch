import { Action } from '@ngrx/store';
import { Movie } from '../movie';

export const ADD = '[MyMovies] Add';
export const REMOVE = '[MyMovies] Remove';

export function myMoviesReducer(state = [], action: Action): Movie[] {
  switch (action.type) {
    case ADD:
      if (!state.find(movie => movie.title === action.payload.title)) {
        state.push(action.payload);
      }
      return state;
    case REMOVE:
      let i;
      state.forEach((movie, index) => {
        if(action.payload.title === movie.title) {
          i = index;
        }
      });
      state.splice(i, 1);
      return state;
    default:
      return state;
  }
}
