import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyMoviesComponent } from './my-movies.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [MyMoviesComponent],
  exports: [MyMoviesComponent]
})
export class MyMoviesModule { }
