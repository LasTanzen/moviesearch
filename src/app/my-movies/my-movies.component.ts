import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../app-state';
import { Movie } from '../movie';
import { REMOVE } from './my-movies.reducer';

@Component({
  selector: 'app-my-movies',
  templateUrl: './my-movies.component.html',
  styleUrls: ['./my-movies.component.css']
})
export class MyMoviesComponent implements OnInit {

  myMovies$ = this.store.select(state => state.myMovies);

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  onMovieSelect(movie: Movie) {
    this.store.dispatch({
      type: REMOVE,
      payload: movie
    });
  }

}
